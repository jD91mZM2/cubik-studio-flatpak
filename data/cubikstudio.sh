#!/usr/bin/env bash
set -euo pipefail

data="${XDG_DATA_HOME:-$HOME/.local/share}/cubikstudio"

# Install data to writeable location
if [ ! -e "$data" ]; then
    cp -r /app/share/cubikupdater "$data"
fi

if [ -f "$data/cubikstudio.x86_64" ]; then
    # Lanuch application
    "$data"/cubikstudio.x86_64
else
    # Run updater
    "$data"/cubikupdater.x86_64
fi
