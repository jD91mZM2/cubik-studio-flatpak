#!/usr/bin/env bash
set -euo pipefail

flatpak-builder output studio.cubik.CubikStudio.yaml --force-clean --install --user

if [ "${1-}" == "sh" ]; then
    flatpak run --command=sh --devel studio.cubik.CubikStudio
else
    flatpak run studio.cubik.CubikStudio
fi
