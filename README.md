# Cubik Studio for Flatpak

This is [Cubik Studio](https://cubik.studio/) packaging for flatpak. You must
own Cubik Studio for it to work.

## Reasons for using flatpak

I'm normally a big fan of [Nix](https://nixos.org/), which would normally take
care of reproducibility for us. But I had trouble packaging it using Nix because
of the nature of the application (proprietary binary download, an installer) and
for unknown reasons.

Flatpak for this case is good, because:

- **Sandboxed**. You don't have to put *as much* faith in people to do the right
  thing, although obviously no sandbox is perfect.
- **Corporate-friendly**. They don't state it out loud, but the main reasons
  flatpak or snap exist, is actually to make reproducibility **for companies**.
  After all, if you had full control over the source, why would you need a
  container to set up a virtual filesystem that looks exactly like Ubuntu? Why
  bundle all dependencies instead of distributing them separately. You could
  just modify the source to not make assumptions? No, the reason is that
  companies distribute binaries, and distributed binaries can't know where all
  shared libraries are, unless you modify the filesystem to put them in the
  expected places.

## Usage

1. Download Cubik Studio for Linux (`cubik_updater_linux.zip`) and place it
   (don't unzip it!) in this directory.

1. Add FlatHub remote to Flatpak (if you haven't)

   ```sh
   # installation is system-specific, look that up yourself
   flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
   ```

1. Install all dependencies and build

   ```sh
   flatpak-builder output studio.cubik.CubikStudio.yaml --force-clean --install-deps-from=flathub
   ```

1. Build *and install* the flatpak

   ```sh
   flatpak-builder output studio.cubik.CubikStudio.yaml --force-clean --install --user
   ```

1. Run it

   ``` sh
   flatpak run studio.cubik.CubikStudio
   ```

## License

Obviously, the `MIT` license in this repository **only affects the packaging
configurations**. Cubik Studio itself has its own proprietary license, and is
absolutely not affiliated with this project or its licensing in any way.
